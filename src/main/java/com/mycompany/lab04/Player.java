/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author Admin
 */
public class Player {
    private char symbol;
    public int winCount, loseCount, drawCount;
    
    public Player(char symbol){
        this.symbol = symbol;
        this.winCount = 0;
        this.loseCount = 0;
        this.drawCount = 0;
    }
    public void win() {
        winCount++;
    }
    
    public void lose() {
        loseCount++;
    }
    
    public void draw() {
        drawCount++;
    }


    public char getSymbol() {
        return symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    public void resetStats() {
        winCount = 0;
        loseCount = 0;
        drawCount = 0;
    }
    
}
