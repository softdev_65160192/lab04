/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author Admin
 */
public class Table {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int turnCount = 0;
    private int drawCount = 0;

    public int getDrawCount() {
        return drawCount;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    public Table(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    boolean checkWin() {
        char currentSymbol = currentPlayer.getSymbol();
        return checkRowsAndColumns(currentSymbol) && checkCross(currentSymbol);
    }

    boolean checkDraw() {
        if(turnCount==8) {
            drawCount++;
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        turnCount++;
        if(currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }

    }

    void setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getSymbol();
        }
    }

    private boolean checkRowsAndColumns(char symbol) {
    for (int i = 0; i < 3; i++) {
        //row
        if (table[i][0] == symbol && table[i][1] == symbol && table[i][2] == symbol) {
            return true;
        }
        //col
        if (table[0][i] == symbol && table[1][i] == symbol && table[2][i] == symbol) {
            return true;
        }
    }
    return false;
}

    private boolean checkCross(char symbol) {
        for (int i = 0; i < 3; i++) {
            if ((table[i][0] == symbol && table[i][1] == symbol && table[i][2] == symbol) ||
                (table[0][i] == symbol && table[1][i] == symbol && table[2][i] == symbol)) {
                return true;
            }
        }
        return false;
    }
    private boolean continuePlaying = true;

    public boolean isContinuePlaying() {
        return continuePlaying;
    }

    public void setContinuePlaying(boolean continuePlaying) {
        this.continuePlaying = continuePlaying;
    }

    
}





