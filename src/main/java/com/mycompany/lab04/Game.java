/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class Game {
    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
        newGame();
    }
    
    public void newGame() {
        player1.resetStats(); 
        player2.resetStats();
        this.table = new Table(player1, player2);
    }


    public void play() {
        showWelcome();
        boolean continuePlaying = true;
        while(true) {
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()) {
                showTable();
                printWin();
                Player winner = table.getCurrentPlayer();
                winner.win();
                Player loser;
                if (winner == player1) {
                    loser = player2;
                } else {
                    loser = player1;
                }
                loser.lose();
                break;
            }
            if(table.checkDraw()) {
                showTable();
                printDraw();
                player1.draw();
                player2.draw();
                break;
            }
            table.switchPlayer();
            
        }
        continuePlaying = askToPlayAgain();
        if (continuePlaying) {
            player1.resetStats();
            player2.resetStats();
            showPlayerStatus();
            newGame();
            play();
        }else {
        showPlayerStatus();
    }
}  
    public void showPlayerStatus(){
        System.out.println("Player " + player1.getSymbol() + " - Win: " + player1.getWinCount() + " Lose: " + player1.getLoseCount() + " Draw: " + player1.getDrawCount());
        System.out.println("Player " + player2.getSymbol() + " - Win: " + player2.getWinCount() + " Lose: " + player2.getLoseCount() + " Draw: " + player2.getDrawCount());
    }   
    
    private void showWelcome() {
        System.out.println("Welcome to OX Game!");
    }

    private void showTable() {
        char[][]t = table.getTable();
        for(int row=0; row<3; row++) {
            for(int col=0; col<3; col++) {
                System.out.print(t[row][col] + "");
            }
            System.out.println("");
        }

    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + "Turn");
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please input row col (0-2) : ");
            int row = kb.nextInt();
            int col = kb.nextInt();
            if (invalidInput(row, col)) {
                table.setRowCol(row, col);
                break;
            } else {
            System.out.println("Please enter row and col numbers between 0 and 2.");
        }
    }
   }

    private void printWin() {
        System.out.println("Congratulation! Player "+table.getCurrentPlayer().getSymbol()+" win!");
    }

    private void printDraw() {
        System.out.println("The game ended in the draw!");
    }
    private boolean askToPlayAgain() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Do you want to play again? (yes/no): ");
        String response = kb.nextLine().toLowerCase();
        return response.equals("yes");
    }

    private boolean invalidInput(int row, int col) {
        return row >= 0 && row <= 2 && col >= 0 && col <= 2;
    }

    private Player getCurrentPlayer() {
        return table.getCurrentPlayer();
    }
}
